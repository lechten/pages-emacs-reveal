<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2017-2020,2022 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC-BY-SA-4.0 -->

![Build Status](https://gitlab.com/pages/emacs-reveal/badges/master/build.svg)

---

Example [emacs-reveal] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

This project demonstrates how to use [emacs-reveal] to generate
[reveal.js presentations](https://revealjs.com/)
(HTML slides embedding audio, figures, quizzes, references to bibliography)
from text files in [Org Mode](https://orgmode.org/).
In particular, emacs-reveal aims to simplify the handling of license
information for
[Open Educational Resources (OER)](https://en.wikipedia.org/wiki/Open_educational_resources).
(You do want to publish your presentations with free and open licenses
and you do want to build on work of others, don’t you?  This is not as
easy as it should be; see [this paper](https://dl.gi.de/handle/20.500.12116/24399)
for some ideas of emacs-reveal.)

Head over to the
[Howto Presentation](https://pages.gitlab.io/emacs-reveal/),
which was generated from [this Org file](index.org).

Note that this project is derived from the “real” howto for
emacs-reveal to serve as example for GitLab Pages.  The real howto
might be more current: https://oer.gitlab.io/emacs-reveal-howto/

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: registry.gitlab.com/oer/emacs-reveal/emacs-reveal:9.15.1

pages:
  stage: deploy
  before_script:
    - git submodule sync --recursive
    - git submodule update --init --recursive
  script:
    - emacs --batch --load elisp/publish.el
    - find public -type f -regex '.*\.\(html\|js\|css\)$' -exec gzip -f -k {} \;
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

These build instructions are based on the Docker image `emacs-reveal:9.15.0`
from [this container registry](https://gitlab.com/oer/emacs-reveal/container_registry).
The image contains GNU Emacs with a LaTeX
distribution, [reveal.js](https://revealjs.com/) with selected plugins,
[Org mode](https://orgmode.org/),
[org-re-reveal](https://gitlab.com/oer/org-re-reveal),
[org-re-reveal-ref](https://gitlab.com/oer/org-re-reveal-ref),
[org-re-reveal-citeproc](https://gitlab.com/oer/org-re-reveal-citeproc), and
[oer-reveal](https://gitlab.com/oer/oer-reveal).
During a build, submodules are fetched first, before `emacs` is
executed to publish HTML presentations from `org` files as defined in
[publish.el](elisp/publish.el).

The `find` statement above creates compressed versions of text
resources to reduce page load times.

## Building locally

To work locally with emacs-reveal, two main alternatives are available:
First, install emacs-reveal as part of your daily work environment.
Second, use the Docker image `emacs-reveal`.

### Install emacs-reveal

1. Install emacs-reveal (see its [README](https://gitlab.com/oer/emacs-reveal/-/blob/master/README.md), maybe clone recursively).  Choose a directory, e.g., `~/.emacs.d/elpa`, and clone software
   - `cd ~/.emacs.d/elpa`
   - `git clone https://gitlab.com/oer/emacs-reveal.git`
   - Then, make sure that `emacs-reveal.el` gets loaded in
     your Emacs sessions, e.g., by adding this to your `~/.emacs`:

     ```
     (add-to-list 'load-path "~/.emacs.d/elpa/emacs-reveal")
     (require 'emacs-reveal)
     ```

2. Fork, clone, or download this project
   - Including submodules (`figures`):

     ```
     git clone --recursive https://gitlab.com/pages/emacs-reveal.git
     ```

3. Generate the website: `emacs --batch --load elisp/publish.el`
4. Preview your presentation: `firefox public/index.html`
5. Add/change contents; repeat, starting with step 3.

Read more in the generated presentation.

### Use Docker image emacs-reveal
1. Clone this project

   ```
   git clone --recursive https://gitlab.com/pages/emacs-reveal.git
   ```

2. Start shell in Docker container, e.g.:

   ```
   docker run --rm -it -v $PWD:/oer registry.gitlab.com/oer/emacs-reveal/emacs-reveal:9.15.1
   ```

3. Generate the website in shell in Docker container:

   ```
   cd oer
   emacs --batch --load elisp/publish.el
   ```

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

[ci]: https://about.gitlab.com/gitlab-ci/
[emacs-reveal]: https://gitlab.com/oer/emacs-reveal
[documentation]: https://gitlab.com/oer/emacs-reveal-howto
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

----

Forked from @lechten
