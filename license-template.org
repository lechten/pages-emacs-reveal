# SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

* License Information
  :PROPERTIES:
  :reveal_extra_attr: data-timing="5"
  :reveal_data_state: no-toc-progress
  :HTML_HEADLINE_CLASS: no-toc-progress
  :CUSTOM_ID: license
  :UNNUMBERED: notoc
  :END:

{{{licensepreamble}}}

#+BEGIN_SRC emacs-lisp :results html :exports results
(oer-reveal-license-to-fmt 'html nil nil nil nil nil nil t)
#+END_SRC
#+BEGIN_SRC emacs-lisp :results latex :exports results
(oer-reveal-license-to-fmt 'pdf nil nil nil nil nil nil t)
#+END_SRC
